/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.interfaceproject;

/**
 *
 * @author Acer
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1 = new Human("Ake");
        h1.eat(); 
        h1.walk();
        h1.run();
        h1.speak();
        h1.sleep();
        
        Animal a1=h1;
        System.out.println("h1 is land animal  ? " + (a1 instanceof LandAnimal));
        System.out.println("h1 is reptile animal  ? " + (a1 instanceof Reptile));
        System.out.println("h1 is Poultry animal  ? " + (a1 instanceof Reptile));
        System.out.println("h1 is Aquatic animal  ? " + (a1 instanceof Reptile));
        
        Cat c1 = new Cat("Orange");
        c1.eat(); 
        c1.walk();
        c1.run();
        c1.speak();
        c1.sleep();
        
        Animal a2=c1;
        System.out.println("c1 is land animal  ? " + (a2 instanceof LandAnimal));
        System.out.println("c1 is reptile animal  ? " + (a2 instanceof Reptile));
        System.out.println("c1 is Poultry animal  ? " + (a2 instanceof Poultry));
        System.out.println("c1 is Aquatic animal  ? " + (a2 instanceof AquaticAnimal));
        
        Dog d1 = new Dog("Dimond");
        d1.eat(); 
        d1.walk();
        d1.run();
        d1.speak();
        d1.sleep();
        
        Animal a3=d1;
        System.out.println("d1 is land animal  ? " + (a3 instanceof LandAnimal));
        System.out.println("d1 is reptile animal  ? " + (a3 instanceof Reptile));
        System.out.println("d1 is Poultry animal  ? " + (a3 instanceof Poultry));
        System.out.println("d1 is Aquatic animal  ? " + (a3 instanceof AquaticAnimal));
        
        Crocodile cd1 = new Crocodile("Jorke");
        cd1.eat(); 
        cd1.walk();
        cd1.crawl();
        cd1.speak();
        cd1.sleep();
        
        Animal a4=cd1;
        System.out.println("cd1 is land animal  ? " + (a4 instanceof LandAnimal));
        System.out.println("cd1 is reptile animal  ? " + (a4 instanceof Reptile));
        System.out.println("cd1 is Poultry animal  ? " + (a4 instanceof Poultry));
        System.out.println("cd1 is Aquatic animal  ? " + (a4 instanceof AquaticAnimal));
        
        
        Snake s1 = new Snake ("Nguu");
        s1.eat(); 
        s1.walk();
        s1.crawl();
        s1.speak();
        s1.sleep();
        
        Animal a5=s1;
        System.out.println("s1 is land animal  ? " + (a5 instanceof LandAnimal));
        System.out.println("s1 is reptile animal  ? " + (a5 instanceof Reptile));
        System.out.println("s1 is Poultry animal  ? " + (a5 instanceof Poultry));
        System.out.println("s1 is Aquatic animal  ? " + (a5 instanceof AquaticAnimal));
        
        Fish f1 = new Fish ("Morpla");
        f1.eat(); 
        f1.walk();
        f1.swim();
        f1.speak();
        f1.sleep();
        
        Animal a6=f1;
        System.out.println("f1 is land animal  ? " + (a6 instanceof LandAnimal));
        System.out.println("f1 is reptile animal  ? " + (a6 instanceof Reptile));
        System.out.println("f1 is Poultry animal  ? " + (a6 instanceof Poultry));
        System.out.println("f1 is Aquatic animal  ? " + (a6 instanceof AquaticAnimal));
        
        Crab cr1 = new Crab ("Puunoy");
        cr1.eat(); 
        cr1.walk();
        cr1.swim();
        cr1.speak();
        cr1.sleep();
        
        Animal a7=cr1;
        System.out.println("cr1 is land animal  ? " + (a7 instanceof LandAnimal));
        System.out.println("cr1 is reptile animal  ? " + (a7 instanceof Reptile));
        System.out.println("cr1 is Poultry animal  ? " + (a7 instanceof Poultry));
        System.out.println("cr1 is Aquatic animal  ? " + (a7 instanceof AquaticAnimal));
        
        Bat b1 = new Bat ("KangKaw");
        b1.eat(); 
        b1.walk();
        b1.fly();
        b1.speak();
        b1.sleep();
        
        Animal a8=b1;
        System.out.println("b1 is land animal  ? " + (a8 instanceof LandAnimal));
        System.out.println("b1 is reptile animal  ? " + (a8 instanceof Reptile));
        System.out.println("b1 is Poultry animal  ? " + (a8 instanceof Poultry));
        System.out.println("b1 is Aquatic animal  ? " + (a8 instanceof AquaticAnimal));
        
        Bird b2 = new Bird("Noknoy");
        b2.eat(); 
        b2.walk();
        b2.fly();
        b2.speak();
        b2.sleep();
        
        Animal a9=b2;
        System.out.println("b2 is land animal  ? " + (a9 instanceof LandAnimal));
        System.out.println("b2 is reptile animal  ? " + (a9 instanceof Reptile));
        System.out.println("b2 is Poultry animal  ? " + (a9 instanceof Poultry));
        System.out.println("b2 is Aquatic animal  ? " + (a9 instanceof AquaticAnimal));
        
    }
}

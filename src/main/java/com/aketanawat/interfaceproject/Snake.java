/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.interfaceproject;

/**
 *
 * @author Acer
 */
public class Snake extends Reptile {
    private String nickname;
    public Snake(String nickname){
        super("Snake ",0);
    }

    @Override
    public void crawl() {
       System.out.println("Snake:  " + nickname + " crawl");
    }

      @Override
    public void eat() {
       System.out.println("Snake:  " + nickname + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Snakee:  " + nickname + " can't walk");
    }

    @Override
    public void speak() {
        System.out.println("Snake:  " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Snake:  " + nickname + " sleep");
    }
}

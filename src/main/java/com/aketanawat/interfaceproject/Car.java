/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.interfaceproject;

/**
 *
 * @author Acer
 */
public class Car extends Vahicle implements Runable {

    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startEnngine() {
        System.out.println("Car:  Start engine");
    }

    @Override
    public void stopEnngine() {
        System.out.println("Car:  Stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car: raise speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car: apply break");
    }

    @Override
    public void run() {
        System.out.println("Car:  run");
    }
    
}

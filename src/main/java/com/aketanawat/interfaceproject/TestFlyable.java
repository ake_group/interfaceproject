/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.interfaceproject;

/**
 *
 * @author Acer
 */
public class TestFlyable {
    public static void main(String[] args) {
        Bat bat = new Bat("Kangkaw");
        Bird bird = new Bird("Nok");
        Plane plane = new Plane("Engine 1");
        Dog dog = new Dog("Mha");
        Human human = new Human("Somake");
        Cat cat = new Cat("Orange");
        
        
        Flyable[]flyables = {bat,bird,plane};
        for(Flyable f : flyables){
            if(f instanceof Plane){
                Plane p =(Plane)f;
                p.startEnngine();
                p.run();
            }
            f.fly();
        }
        Runable[]runables = {dog,cat,human,plane};
        for(Runable r : runables){
            r.run();
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.interfaceproject;

/**
 *
 * @author Acer
 */
public class Plane extends Vahicle implements Flyable,Runable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEnngine() {
        System.out.println("Plane:  Start engine");
    }

    @Override
    public void stopEnngine() {

    }

    @Override
    public void raiseSpeed() {

    }

    @Override
    public void applyBreak() {

    }

    @Override
    public void fly() {
        System.out.println("Plane:  fly");
    }

    @Override
    public void run() {
        System.out.println("Plane:  run");
    }

}
